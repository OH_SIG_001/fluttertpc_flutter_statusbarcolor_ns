# flutter_statusbarcolor_ns_ohos

Flutter插件，更改 Flutter 应用的状态栏颜色或导航栏颜色

## Usage

```yaml
dependencies:
  flutter_statusbarcolor: 0.5.0
  flutter_statusbarcolor_ns_ohos: 1.0.1
```

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
ohos

For help getting started with Flutter development, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## 支持格式
|格式|android|ios|   OpenHarmony |
|:----|:----:|:----:|:--------------:|
|getstatusbarcolor| ✅ | ✅ |
|setstatusbarcolor| ✅ | ✅ |
|setstatusbarwhiteforeground| ✅ | ✅ |
|getnavigationbarcolor| ✅ | ✅ |
|setnavigationbarcolor| ✅ | ✅ |
|setnavigationbarwhiteforeground| ✅ | ✅ |
