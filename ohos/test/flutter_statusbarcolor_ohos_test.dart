import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns_ohos/flutter_statusbarcolor_ns_ohos.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  const MethodChannel channel = MethodChannel('plugins.sameer.com/statusbar');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getstatusbarcolor') {
        return 0xFFFFFFFF;
      } else if (methodCall.method == 'setstatusbarcolor') {
        return null;
      } else if (methodCall.method == 'setstatusbarwhiteforeground') {
        return null;
      } else if (methodCall.method == 'getnavigationbarcolor') {
        return 0xFF000000;
      } else if (methodCall.method == 'setnavigationbarcolor') {
        return null;
      } else if (methodCall.method == 'setnavigationbarwhiteforeground') {
        return null;
      }
      return null;
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getStatusBarColor', () async {
    final Color? color = await FlutterStatusbarcolor.getStatusBarColor();
    expect(color, isNotNull);
    expect(color, equals(Colors.white));
  });

  test('setStatusBarColor', () async {
    await FlutterStatusbarcolor.setStatusBarColor(Colors.blue);
  });

  test('setStatusBarWhiteForeground', () async {
    await FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
  });

  test('getNavigationBarColor', () async {
    final Color? color = await FlutterStatusbarcolor.getNavigationBarColor();
    expect(color, isNotNull);
    expect(color, equals(Colors.black));
  });

  test('setNavigationBarColor', () async {
    await FlutterStatusbarcolor.setNavigationBarColor(Colors.red);
  });

  test('setNavigationBarWhiteForeground', () async {
    await FlutterStatusbarcolor.setNavigationBarWhiteForeground(true);
  });
}
